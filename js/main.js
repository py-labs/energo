var status11 = {
        code: '11',
        action: function (el) {
            // nothing
        }
    },
    status12 = {
        code: '12',
        action: function (el) {
            el.style('stroke-dasharray', 9);
        }
    },
    status21 = {
        code: '21',
        action: function (el) {
            el.rotate(90)
        }
    },
    status22 = {
        code: '22',
        action: function (el) {
            el.rotate(90);
            el.style('stroke-dasharray', 9)
        }
    },
    status25 = {
        code: '25',
        action: function (el) {
            var errorId = el.attr().id + '-error',
                errorEl = getErrorElement(errorId);
            el.rotate(90);
            el.style('stroke-dasharray', 9);
            errorEl.el.show();
            if (errorEl.el.x() < errorEl.coordX + 8)
                errorEl.el.dx(8);
        }
    },
    status15 = {
        code: '15',
        action: function (el) {
            var errorId = el.attr().id + '-error',
                errorEl = getErrorElement(errorId);
            el.style('stroke-dasharray', 9);
            errorEl.el.show();
        }
    },
    status45 = {
        code: '45',
        action: function (el) {
            var errorId = el.attr().id + '-error',
                errorEl = getErrorElement(errorId);
            el.hide();
            errorEl.el.show();
            if (errorEl.el.x() < errorEl.coordX + 8)
                errorEl.el.dx(8);

        }
    },
    statuses = [status11, status12, status15, status21, status22, status25, status45],
    locks = {101: 'lock1', 102: 'lock2', 103: 'lock3', 104: 'lock4', 105: 'lock5'},
    errorLocks = fillErrorElements();


function fillErrorElements() {
    var elements = [];
    for (var lock in locks) {
        if (locks.hasOwnProperty(lock)) {
            var lockName = locks[lock];
            var el = SVG.get(lockName + '-error');
            var lockErrorElement = {
                id: lockName + '-error',
                el: el,
                coordX: el.x()
            };
            elements.push(lockErrorElement);
        }
    }
    return elements
}

function getErrorElement(id) {
    for (var i = 0; i < errorLocks.length; i++) {
        var errorElement = errorLocks[i];
        if (errorElement.id === id)
            return errorElement
    }
    return null
}


function checkLocks(text) {
    for (var lock in text) {
        if (text.hasOwnProperty(lock) && locks.hasOwnProperty(lock)) {
            var status_code = text[lock],
                idLock = locks[lock];
            for (var i = 0; i < statuses.length; i++) {
                var status = statuses[i];
                if (status.code === status_code) {
                    status.action(SVG.get(idLock));
                    break;
                }
            }
        }
    }
}


function main() {
    if (!SVG.supported) {
        alert('SVG not supported');
        return;
    }

    var testText = {102: '11', 103: '12', 101: '21', 104: '22', 105: '45'};

    SVG.on(document, 'DOMContentLoaded', function () {
        document.getElementById("manage_btn").onclick = function () {
            // $.get( "/file", function( data ) {
            //     if (data)
            //         testText = data;
            //     checkLocks(testText);
            // });
            checkLocks(testText);

        }
    })
}


main();
